#!/bin/sh

# Copyright (C) 2020-2021 cosimone64

# This program is free software: you can redistribute it and/or
# modify it under the terms of the GNU Affero General Public License
# as published by the Free Software Foundation, either version 3 of
# the License, or (at your option) any later version.

# You should have received a copy of the GNU AGPLv3 with this software,
# if not, please visit <https://www.gnu.org/licenses/>

# Note: this script should be run with superuser privileges.
apt-get update
apt-get upgrade

# Set up unattended upgrades
apt-get install unattended-upgrades apt-listchanges

printf "Unattended-Upgrade::Origins-Pattern {\n\
        \"origin=Debian,codename=${distro_codename},label=Debian-Security\";\n\
    \"origin=TorProject\";\n\
};\n\
Unattended-Upgrade::Package-Blacklist {\n\
};\n\
Unattended-Upgrade::Automatic-Reboot \"true\";" \
       > /etc/apt/apt.conf.d/50unattended-upgrades

printf "APT::Periodic::Update-Package-Lists \"1\";\n\
APT::Periodic::AutocleanInterval \"5\";\n\
APT::Periodic::Unattended-Upgrade \"1\";\n\
APT::Periodic::Verbose \"1\";" > /etc/apt/apt.conf.d/20auto-upgrades

# Enable Tor Debian repository (for latest versions) and install Tor.

apt-get install apt-transport-https
release=$(lsb_release -cs)
printf "deb     https://deb.torproject.org/torproject.org $release main\n\
deb-src https://deb.torproject.org/torproject.org $release main" \
       > /etc/apt/sources.list.d/torrepo
wget -qO- https://deb.torproject.org/torproject.org/A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89.asc | gpg --import
gpg --export A3C4F0F979CAA22CDBA8F512EE8CBC9E886DDD89 | apt-key add -
apt-get update
apt-get install tor deb.torproject.org-keyring

nickname="nickname"
email="email@domain.org"
printf "Nickname $nickname\n\
ORPort 443\n\
ExitRelay 0\n\
SocksPort 0\n\
ControlSocket 0\n\
ContactInfo $email" > /etc/tor/torrc

# Install some additional tools
apt-get install iftop nyx

# Enable and start services
systemctl enable tor
systemctl restart tor
systemctl restart tor@default
systemctl enable unattended-upgrades
systemctl restart unattended-upgrades

# Finally test unattended-upgrade, possibly rebooting
unattended-upgrade -d
